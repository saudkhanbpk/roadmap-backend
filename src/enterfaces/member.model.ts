import { Task } from './task.model';
export interface Member {
  key: number;
  id: string;
  name: string;
  memberdata: Task;
}
